# time saver and productivity booster

from datetime import datetime as dt
import time
from_time=8
until_time=16

hosts_path = "/etc/hosts"
redirect = '127.0.0.1'
website_list = ["www.facebook.com","facebook.com","youtube.com","www.youtube.com","jalopnik.com","www.olx.ba"]

while True:
    if dt(dt.now().year,dt.now().month,dt.now().day,from_time) < dt.now() < dt(dt.now().year,dt.now().month,dt.now().day,until_time):
        with open(hosts_path,"r+") as file:
            hfdata = file.read()
            for website in website_list:
                if website in hfdata:
                    pass
                else:
                    file.write(redirect + " " + website + "\n")
    else:
        with open(hosts_path,"r+") as file:
            hfdata = file.readlines()
            file.seek(0)
            for line in hfdata:
                if not any(website in line for website in website_list):
                    file.write(line)
            file.truncate()

    time.sleep(5)